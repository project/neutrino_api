<?php

namespace Drupal\Tests\neutrino_api_ip_info\Kernel;

use Drupal\KernelTests\KernelTestBase;
use NeutrinoAPI\APIResponse;

/**
 * Test cases for the Neutrino ip info service.
 *
 * @group neutrino_api_ip_info
 */
class IpInfoTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'neutrino_api',
    'neutrino_api_test',
    'neutrino_api_ip_info',
  ];

  /**
   * Known valid data for the neutrino ip info service.
   */
  protected const VALID_RESPONSE_DATA = [
    'region-code' => 'BE',
    'country' => 'Germany',
    'country-code' => 'DE',
    'city' => 'Berlin',
    'timezone' => [
      'date' => '2023-11-29',
      'offset' => '+01:00',
      'name' => 'Central European Standard Time',
      'id' => 'Europe/Berlin',
      'time' => '18:35:04.726376',
      'abbr' => 'CET',
    ],
    'ip' => '85.214.132.117',
    'latitude' => 52.524525,
    'valid' => TRUE,
    'is-v4-mapped' => FALSE,
    'hostname' => '',
    'continent-code' => 'EU',
    'host-domain' => '',
    'currency-code' => 'EUR',
    'region' => 'Berlin',
    'is-bogon' => FALSE,
    'country-code3' => 'DEU',
    'is-v6' => FALSE,
    'longitude' => 13.410037,
  ];

  /**
   * Known invalid data for the neutrino ip info service.
   */
  protected const INVALID_RESPONSE_DATA = [
    'region-code' => '',
    'country' => '',
    'country-code' => '',
    'city' => '',
    'timezone' => [],
    'ip' => '812.123.123.123.123',
    'latitude' => '',
    'valid' => FALSE,
    'is-v4-mapped' => FALSE,
    'hostname' => '',
    'continent-code' => '',
    'host-domain' => '',
    'currency-code' => '',
    'region' => '',
    'is-bogon' => FALSE,
    'country-code3' => '',
    'is-v6' => FALSE,
    'longitude' => '',
  ];

  /**
   * The ip info service.
   *
   * @var \Drupal\neutrino_api_ip_info\NeutrinoIpInfoInterface
   */
  protected $ipInfo;

  /**
   * The mock neutrino client.
   *
   * @var \Drupal\neutrino_api_test\MockNeutrinoClient
   */
  protected $mockClient;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->ipInfo = \Drupal::service('neutrino_api_ip_info.ipinfo');
    $this->mockClient = \Drupal::service('neutrino_api.client');
  }

  /**
   * Test case for a valid ip address.
   */
  public function testIpInfoValidIp() {

    $valid_response = APIResponse::ofData(
      200,
      'json',
      static::VALID_RESPONSE_DATA
    );
    $this->mockClient->setApiResponse($valid_response);

    static::assertSame(
      static::VALID_RESPONSE_DATA,
      $this->ipInfo->getIpInfoMetadata('85.214.132.117')
    );

  }

  /**
   * Test case for an invalid ip address.
   */
  public function testIpInfoInvalidIp() {

    $valid_response = APIResponse::ofData(
      200,
      'json',
      static::INVALID_RESPONSE_DATA
    );
    $this->mockClient->setApiResponse($valid_response);

    static::assertSame(
      static::INVALID_RESPONSE_DATA,
      $this->ipInfo->getIpInfoMetadata('812.123.123.123.123')
    );

  }

}
