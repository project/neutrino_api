<?php

namespace Drupal\neutrino_api_email_validator;

use Drupal\Component\Utility\EmailValidatorInterface;

/**
 * Extends the core email validator interface with typo correction.
 */
interface NeutrinoEmailValidatorInterface extends EmailValidatorInterface {

  /**
   * Gets an array of validation data for the provided email address.
   *
   * @param string $email
   *   The email address to get validation data for.
   *
   * @return array
   *   An associative array of validation metadata.
   */
  public function getAllValidationMetadata($email);

}
