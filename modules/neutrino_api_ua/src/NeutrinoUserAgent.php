<?php

namespace Drupal\neutrino_api_ua;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use NeutrinoAPI\NeutrinoAPIClient;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * This service taps the Neutrino API user agent service.
 */
class NeutrinoUserAgent implements NeutrinoUserAgentInterface {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The cache factory service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The client factory service.
   *
   * @var \NeutrinoAPI\NeutrinoAPIClient
   */
  protected $client;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $requestStack;

  /**
   * Creates a neutrino user agent service instance.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache factory service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger service.
   * @param \NeutrinoAPI\NeutrinoAPIClient $client
   *   The client factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   */
  public function __construct(CacheBackendInterface $cache, TimeInterface $time, LoggerChannelInterface $logger, NeutrinoAPIClient $client, RequestStack $request_stack) {
    $this->cache = $cache;
    $this->time = $time;
    $this->logger = $logger;
    $this->client = $client;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserAgentMetadata($ua = NULL) {

    if (!$ua) {
      // If there is no user agent passed through, pull from the user's request.
      if ($request = $this->requestStack->getCurrentRequest()) {
        // Get the user agent from the request object.
        $ua = $request->headers->get('User-Agent', '');
      }
    }

    if ($this->cache->get($ua) === FALSE) {
      $response = $this->client->uaLookup(['ua' => $ua]);
      if ($response->isOK()) {
        // Discuss timing for this cache.
        $this->cache->set($ua, $response->getData(), $this->time->getCurrentTime() + 60);
      }
      else {
        $this->logger->error($response->getErrorMessage());
      }
    }

    return $this->cache->get($ua)->data ?? [];
  }

}
