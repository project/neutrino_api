<?php

namespace Drupal\Tests\neutrino_api_ua\Kernel;

use Drupal\KernelTests\KernelTestBase;
use NeutrinoAPI\APIResponse;

/**
 * Test cases for the Neutrino UA lookup service.
 *
 * @group neutrino_api_ua
 */
class UserAgentTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'neutrino_api',
    'neutrino_api_test',
    'neutrino_api_ua',
  ];

  /**
   * Known valid data for the neutrino ua lookup service.
   */
  protected const VALID_RESPONSE_DATA = [
    'device-model' => '',
    'os' => 'Linux',
    'device-brand' => '',
    'browser-release' => '2024',
    'os-family' => 'Linux',
    'device-pixel-ratio' => 1,
    'device-height-px' => 0,
    'ua' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36',
    'type' => 'desktop',
    'device-ppi' => 96,
    'device-price' => 0,
    'version' => '121.0.0.0',
    'version-major' => '121',
    'os-version-major' => '',
    'browser-engine' => 'Blink',
    'device-model-code' => '',
    'os-version' => '',
    'device-release' => '',
    'name' => 'Chrome',
    'is-webview' => FALSE,
    'device-resolution' => '',
    'device-width-px' => 0,
    'is-mobile' => FALSE,
  ];

  /**
   * Known invalid data for the neutrino ua lookup service.
   */
  protected const INVALID_RESPONSE_DATA = [
    'device-model' => '',
    'os' => '',
    'device-brand' => '',
    'browser-release' => '',
    'os-family' => '',
    'device-pixel-ratio' => 1,
    'device-height-px' => 0,
    'ua' => 'this-is-not-a-standard-ua',
    'type' => 'unknown',
    'device-ppi' => 96,
    'device-price' => 0,
    'version' => '',
    'version-major' => '',
    'os-version-major' => '',
    'browser-engine' => '',
    'device-model-code' => '',
    'os-version' => '',
    'device-release' => '',
    'name' => '',
    'is-webview' => FALSE,
    'device-resolution' => '',
    'device-width-px' => 0,
    'is-mobile' => FALSE,
  ];

  /**
   * The neutrino user agent service.
   *
   * @var \Drupal\neutrino_api_ua\NeutrinoUserAgent
   */
  protected $neutrinoUserAgent;

  /**
   * The mock neutrino client.
   *
   * @var \Drupal\neutrino_api_test\MockNeutrinoClient
   */
  protected $mockClient;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->neutrinoUserAgent = \Drupal::service('neutrino_api_ua.lookup');
    $this->mockClient = \Drupal::service('neutrino_api.client');
  }

  /**
   * Test case for a valid user agent.
   */
  public function testValidUserAgent() {

    $valid_response = APIResponse::ofData(
      200,
      'json',
      static::VALID_RESPONSE_DATA
    );
    $this->mockClient->setApiResponse($valid_response);

    static::assertSame(
      static::VALID_RESPONSE_DATA,
      $this->neutrinoUserAgent->getUserAgentMetadata('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36')
    );

  }

  /**
   * Test case for an invalid user agent.
   */
  public function testInvalidUserAgent() {

    $valid_response = APIResponse::ofData(
      200,
      'json',
      static::INVALID_RESPONSE_DATA
    );
    $this->mockClient->setApiResponse($valid_response);

    static::assertSame(
      static::INVALID_RESPONSE_DATA,
      $this->neutrinoUserAgent->getUserAgentMetadata('this-is-not-a-standard-usa')
    );

  }

}
