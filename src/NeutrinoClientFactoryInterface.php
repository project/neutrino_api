<?php

namespace Drupal\neutrino_api;

/**
 * Factory helper service to retrieve a third party API client.
 *
 * The factory pattern allows for unit-test-friendly mocks to be used.
 */
interface NeutrinoClientFactoryInterface {

  /**
   * Creates a Neutrino API client instance.
   *
   * @return \NeutrinoAPI\NeutrinoAPIClient
   *   A Neutrino API client instance.
   */
  public function create();

}
