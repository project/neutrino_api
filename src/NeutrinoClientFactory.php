<?php

namespace Drupal\neutrino_api;

use Drupal\Core\Config\ConfigFactoryInterface;
use NeutrinoAPI\NeutrinoAPIClient;

/**
 * Factory helper service to retrieve a third party API client.
 *
 * The factory pattern allows for unit-test-friendly mocks to be used.
 */
class NeutrinoClientFactory implements NeutrinoClientFactoryInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates a Neutrino client factory.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function create() {
    $config = $this->configFactory->get('neutrino_api.settings');
    return new NeutrinoAPIClient(
      $config->get('user_id') ?? '',
      $config->get('api_key') ?? ''
    );
  }

}
