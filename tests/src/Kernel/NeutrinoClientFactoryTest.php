<?php

namespace Drupal\Tests\neutrino_api\Kernel;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactory;
use Drupal\KernelTests\KernelTestBase;
use NeutrinoAPI\NeutrinoAPIClient;

/**
 * Test cases for the neutrino api client factory.
 *
 * @group neutrino_api
 */
class NeutrinoClientFactoryTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'neutrino_api',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $config = $this->createMock(Config::class);
    $config->method('get')->willReturn('Dummy Value');
    $config_factory = $this->createMock(ConfigFactory::class);
    $config_factory->method('get')->willReturn($config);

    $this->container->set('config.factory', $config_factory);
  }

  /**
   * Test case for the neutrino api client factory.
   */
  public function testNeutrinoClientFactory() {
    static::assertInstanceOf(
      NeutrinoAPIClient::class,
      \Drupal::service('neutrino_api.client')
    );
  }

}
